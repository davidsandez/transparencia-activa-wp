<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

// Filtro para que todos los resultados sean CPT Datasets y esten ordenados alfabeticamente
add_filter('pre_get_posts', function($query) {
    if ($query->is_search && !is_admin() ) {
        $query->set('posts_per_page', -1);
        $query->set('post_type',array('dataset'));
        $query->set('order', 'ASC');
        $query->set('orderby','post_title');
    }
    return $query;
}, 10,2); 

// Eliminar el Continued de los resultados con varios caracteres
add_filter('excerpt_more', function() {
    global $post;
    return '…';
}, 10,1);

//Modificar el titulo de Search si se busca por facets o sin string
add_filter( 'the_seo_framework_title_from_generation', function( $title, $args ) { 
    if(is_search()){ 
        if(!empty(get_search_query())){ 
            return sprintf(__('Resultados de búsqueda para "%s"', 'sage'), get_search_query()); 
        }else{ 
            if(isset($_GET["_organismos"])){
                return sprintf(__('Resultados de búsqueda por Organismos', 'sage'));
            }
            if(isset($_GET["_categorias"])){
                return sprintf(__('Resultados de búsqueda por Categorías', 'sage'));
            }
            if(isset($_GET["_formatos"])){
                return sprintf(__('Resultados de búsqueda por Formatos', 'sage'));
            }
            if(empty(get_search_query()) && !isset($_GET["_organismos"]) && !isset($_GET["_categorias"]) && !isset($_GET["_formatos"])){ 
                return sprintf(__('Resultados de búsqueda', 'sage'));
            }
        }
    }
}, 10, 2 );