<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
	public static function categoriasLoop()
	{ 
		$categoriasLoop = get_terms( array(
		    'taxonomy' => 'categoria',
		    'hide_empty' => false
		) );
		return $categoriasLoop;
	} 
	public static function organismosLoop()
	{ 
		$organismosLoop = get_terms( array(
		    'taxonomy' => 'organismo',
		    'hide_empty' => false
		) );
		return $organismosLoop;
	} 
}
