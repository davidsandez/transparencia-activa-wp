<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Search extends Controller
{  
	public function variableResultado(){  
	   global $wp_query;
	   return get_search_query();
	}
	public function cantidadResultados(){
	   global $wp_query;
	   $not_singular = $wp_query->found_posts > 1 ? ' resultados' : ' resultado';
	   return $wp_query->found_posts . $not_singular;
	}
	public static function formatoBucle($post){  
	    $terms = wp_get_post_terms( $post->ID, 'formato');
	    return $terms;
	}
	public static function organismoBucle($post){  
	    $terms = wp_get_post_terms( $post->ID, 'organismo');
	    return $terms;
	}
	public static function categoriaBucle($post){  
	    $terms = wp_get_post_terms( $post->ID, 'categoria');
	    return $terms;
	}
}
