<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use PostTypes\PostType;
use PostTypes\Taxonomy;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]; 
    register_sidebar([
        'name'          => __('Footer Top', 'sage'),
        'id'            => 'sidebar-top-footer'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer Bottom', 'sage'),
        'id'            => 'sidebar-bottom-footer'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer 1', 'sage'),
        'id'            => 'sidebar-footer-1'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer 2', 'sage'),
        'id'            => 'sidebar-footer-2'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer 3', 'sage'),
        'id'            => 'sidebar-footer-3'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer 4', 'sage'),
        'id'            => 'sidebar-footer-4'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

add_action('migas_de_pan', function () {
    if(function_exists('bcn_display') && is_front_page() == false){
        bcn_display();
    }

});

/* Custom Blocks */
add_action('acf/init', function() {
  if( function_exists('acf_register_block') ) {
    // Look into views/blocks
    $dir = new \DirectoryIterator(\locate_template("views/blocks/"));
    // Loop through found blocks
    foreach ($dir as $fileinfo) {
      if (!$fileinfo->isDot()) {
        $slug = str_replace('.blade.php', '', $fileinfo->getFilename());
        // Get infos from file
        $file_path = \locate_template("views/blocks/${slug}.blade.php");
        $file_headers = get_file_data($file_path, [
          'title' => 'Title',
          'description' => 'Description',
          'category' => 'Category',
          'icon' => 'Icon',
          'keywords' => 'Keywords',
        ]);
        if( empty($file_headers['title']) ) {
          die( _e('This block needs a title: ' . $file_path));
        }
        if( empty($file_headers['category']) ) {
          die( _e('This block needs a category: ' . $file_path));
        }
        // Register a new block
        $datas = [
          'name' => $slug,
          'title' => $file_headers['title'],
          'description' => $file_headers['description'],
          'category' => $file_headers['category'],
          'icon' => $file_headers['icon'],
          'keywords' => explode(' ', $file_headers['keywords']),
          'render_callback'  => 'my_acf_block_render_callback'
        ];
        acf_register_block($datas);
      }
    }
  }
});  

add_action( 'wp_head', function() {
?>
    <script>
    (function($) {

      $(document).on('facetwp-refresh', function() {
        if (FWP.loaded) {
          FWP.set_hash();
          window.location.reload();
          return false;
        }
      }); 

      $(document).on('facetwp-loaded', function() { // on each refresh

        $('.facetwp-checkbox').each(function() { // foreach "style" checkbox option
          var val = $(this).attr('data-value'); // grab its "data-value" attribute

          if ('excel' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--excel"><i class="fas fa-file-excel"></i></span>';
          }

          else if ('pdf' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--pdf"><i class="fas fa-file-pdf"></i></span>';
          }

          else if ('csv' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--csv"><i class="fas fa-file-csv"></i></span>';
          }

          else if ('imagen' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--imagen"><i class="fas fa-file-image"></i></span>';
          }

          else if ('powerpoint' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--powerpoint"><i class="fas fa-file-powerpoint"></i></span>';
          }

          else if ('sitio-web' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--sitio-web"><i class="fas fa-globe"></i></span>';
          }

          else if ('word' == val) { // is the value "black"?
            var img = '<span class="icono-checkbox icono-checkbox--word"><i class="fas fa-file-word"></i></span>';
          }

          else if ('administracion' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-pen-nib"></i></span>';
          }

          else if ('acordadas-y-resoluciones' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-file-alt"></i></span>';
          }

          else if ('concursos' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-balance-scale"></i></span>';
          }

          else if ('institucional' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-network-wired"></i></span>';
          }

          else if ('participacion-ciudadana' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-users"></i></span>';
          }

          else if ('sentencias-y-dictamenes' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-gavel"></i></span>';
          }

          else if ('presupuesto' == val) { // is it "white"?
            var img = '<span class="icono-checkbox"><i class="fas fa-dollar-sign"></i></span>';
          }

          else if ('consejo-de-la-magistratura' == val) { // is it "white"?
            var img = '<span class="item__organismos item__organismos--cdm"></span>';
          }

          else if ('ministerio-publico-de-la-defensa' == val) { // is it "white"?
            var img = '<span class="item__organismos item__organismos--def"></span>';
          }

          else if ('ministerio-publico-fiscal' == val) { // is it "white"?
            var img = '<span class="item__organismos item__organismos--mpf"></span>';
          }

          else if ('ministerio-publico-tutelar' == val) { // is it "white"?
            var img = '<span class="item__organismos item__organismos--mpt"></span>';
          }

          else if ('tribunal-superior-de-justicia' == val) { // is it "white"?
            var img = '<span class="item__organismos item__organismos--tsj"></span>';
          }


          
          // add more "else if" statements as needed

          $(this).prepend(img); // replace checkbox content with the image
        });
      });
    })(jQuery);
    </script>
<?php
}, 100 );

add_post_type_support( 'page', 'excerpt', 'editor' );

/**
 * Inject critical assets in head as early as possible
 */
add_action('wp_head', function (): void {
    if ('development' === env('WP_ENV')) {
        return;
    }

    if (is_front_page()) {
        // $critical_CSS = locate_asset('styles/critical-home.css');
        $critical_CSS = 'styles/critical-home.css';
    } elseif (is_singular()) {
        // $critical_CSS = locate_asset('styles/critical-singular.css');
        $critical_CSS = 'styles/critical-singular.css';
    } else {
        // $critical_CSS = locate_asset('styles/critical-landing.css');
        $critical_CSS = 'styles/critical-landing.css';
    }

    // if (file_exists($critical_CSS)) {
    if (file_exists(locate_asset($critical_CSS))) {
        echo '<style id="critical-css">' . get_file_contents($critical_CSS) . '</style>';
    }
}, 1);


/*Custom post type datasets*/
$datasets = [
  'name'     => 'dataset',
  'singular' => 'Dataset',
  'plural'   => 'Datasets',
  'slug'     => 'datasets',
];

$datasets = new PostType( $datasets ); 

$datasets->options( [
  'has_archive' => true,
  'show_in_rest' => true,
  'public' => true, 
  'supports'  => array( 'editor', 'title', 'excerpt')
] ); 

// Add the default category taxonomy.
$datasets->taxonomy( 'organismo' );

$datasets->labels( [
  'add_new'               => __( 'Agregar nuevo', 'transparencia' ),
  'add_new_item'          => __( 'Agregar nuevo Dataset', 'transparencia' ),
  'not_found'             => __( 'No se encontro nada.', 'transparencia' ),
  'view_item'             => __( 'Ver Dataset', 'textdomain' ),
]); 
$datasets->icon( 'dashicons-chart-bar' );
$datasets->register();

$options = [
  'hierarchical' => true, 
  "show_in_rest" => true,
]; 

// Nueva taxonomia categorias
$categorias = new Taxonomy('categoria', $options);

// Asigno a Datasets
$categorias->posttype( 'dataset' );

// Registro categorias
$categorias->register();

// Creo taxonomia organismos
$organismos = new Taxonomy('organismo', $options);

// Asigno a Datasets
$organismos->posttype( 'dataset' );

// Registro organsimos
$organismos->register();

// Creo taxonomía formato
$formato = new Taxonomy('formato', $options);

// Asigno a Datasets
$formato->posttype( 'dataset' );

// Registro formato
$formato->register();


if ( function_exists( 'add_image_size' ) ) {  
  add_image_size( 'homepage-thumb', 350, 150, true ); //(cropped)
  add_image_size( 'homepage-hero', 1900, 680, true ); //(cropped)
}


