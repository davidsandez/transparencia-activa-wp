<article @php post_class() @endphp>
    <div class="migas-de-pan">
        <div class="container">
        @php 
        do_action('migas_de_pan');
        @endphp
    </div>
    </div>
   <section class="contenido-estatico">
        <div class="container">
            <div class="row">
                <h3 class="titulo-estatico">{!! get_the_title() !!}</h3>
                <div class="texto-estatico">@php the_content() @endphp</div>  
            </div>
        </div>
    </section> 
  </footer> 
</article>

   
   