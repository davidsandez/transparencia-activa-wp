{{--
	Title: Hero
	Description: Hero 
	Category: formatting
	Icon: admin-comments
	Keywords: hero
	Mode: edit
	Align: left
	PostTypes: page post
	SupportsAlign: left right
	SupportsMode: false
	SupportsMultiple: false
--}}

<!-- Hero -->
<section class="hero" data-{{ $block['id'] }} class="{{ $block['classes'] }}">
	<div class="container">
	    <div class="row d-flex justify-content-center align-items-center">
	        <h1 class="hero__titulo">{{ get_field('titulo') }}</h1>
	        <p class="hero__texto">{!! get_field('descripcion') !!}</p> 
	            <form role="search" method="get" class="input-group mb-3 hero__buscador" action="{{ home_url('/') }}">
	                <input type="text" class="form-control" placeholder="Ingresá un término de búsqueda: Sentencias, Presupuesto..." name="s" >
	                <div class="input-group-append">
	                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
	                </div>
	            </form> 

	    </div>
	</div>
	<div class="hero__img"><img src="{{ get_field('imagen') }}" alt="{{ get_field('titulo') }}" /></div>
</section>