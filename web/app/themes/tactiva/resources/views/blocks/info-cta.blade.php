{{--
	Title: Información CTA
	Description: info 
	Category: formatting
	Icon: admin-comments
	Keywords: hero
	Mode: edit
	Align: left
	PostTypes: page post
	SupportsAlign: left right
	SupportsMode: false
	SupportsMultiple: false
--}}

<!-- Formulario de acceso a la información -->
<section class="acceso-informacion" data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <div class="container">
        <h3 class="tituloModulo">{{ get_field('titulo') }}</h3> 
        <div class="contenidoModulo">{!! get_field('descripcion') !!}</div>
        </div>
        <a class="btn btn-primary btnTransparencia" href="{{ get_field('cta-href') }}">{{ get_field('cta-text') }}</a>
    </div>
</section>