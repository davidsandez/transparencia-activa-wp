@extends('layouts.app')

@section('content')
    @isset($_GET['_organismos']) 
        @php    
            $organismo = get_term_by('slug', $_GET['_organismos'], 'organismo');  
        @endphp
        @if($organismo)
            @php   
                $titulo = $organismo->name;
                $slug = $organismo->slug;
                $direccion = get_field('direccion', $organismo);
                $telefono = get_field('telefono', $organismo);
                $url = get_field('url', $organismo);
                $imagen = get_field('imagen', $organismo);
                $menu = get_field('menu', $organismo);  
                $colorfondo = get_field('clase_menu', $organismo);  
            @endphp 
            <section class="hero__landing hero__landing-organismo">
                <div class="hero__img--landing"><img src="{{$imagen}}"></div>
                <div class="container">
                    <div class="row">
                        <div class="contenedor-organismos">
                        <h1 class="hero__titulo">{{$titulo}}</h1>
                        <p class="hero__informacion">
                            <span class="icono-organismos"><i class="fas fa-map-marker-alt"></i></span><span class="item-organismo">{{$direccion}}</span>
                            <span class="icono-organismos"><i class="fas fa-phone-alt"></i></span><span class="item-organismo">{{$telefono}}</span>
                            <span class="icono-organismos"><i class="fas fa-link"></i></span><a href="{{$url}}" target="_blank"><span class="item-organismo">{{$url}}</a></span>
                        </p>
                        </div>
                    </div>
                </div>
            </section>   
            <section class="{{$slug}}__navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light navbar-organismos--menu">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <?php wp_nav_menu( array(
                                        'menu' => $menu,
                                        'depth' => 2,
                                        'container' => '',
                                        'container_class' => '',
                                        'menu_class' => 'collapse navbar-collapse',
                                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker'          => new WP_Bootstrap_Navwalker(),
                                         ) );?>
                        </div>
                    </nav>
                </div>
            </section> 
        @endif 
    @endisset 
<div class="container">
        <div class="row">
            <div class="col-lg-4 col-12">
                <div id="sidebar"> 
                    @empty(!$variable_resultado)
                        <h3 class="container-busqueda__title">"{{ $variable_resultado }}"</h3>
                    @endempty 
                    <?php echo do_shortcode('[facetwp facet="resultados"]'); ?>
                    <?php echo do_shortcode('[facetwp selections="true"]'); ?>
                    <div class="widget-categorias">
                        <h3 class="titulo-side-bar">Categorías</h3>
                        <ul class="lista__categorias"> 
                            <?php echo do_shortcode('[facetwp facet="categorias"]'); ?> 
                        </ul>
                    </div>
                    <div class="widget-organismos">
                        <h3 class="titulo-side-bar">Organismos</h3>
                        <ul class="lista__organismos">
                            <?php echo do_shortcode('[facetwp facet="organismos"]'); ?>
                        </ul>
                    </div>
                    <div class="widget-formatos">
                        <h3 class="titulo-side-bar">Formatos</h3>
                        <ul class="lista-formatos">
                            <?php echo do_shortcode('[facetwp facet="formatos"]'); ?>
                        </ul>
                    </div>

                    
                    
                </div>
            </div>
            <div class="col-lg-8 col-12">
                <div id="contenido-busqueda">
                    <div class="input-group mb-3">  
                    <form role="search" method="get" class="input-group mb-3 hero__buscador" action="{{ home_url('/') }}">
                        <input type="text" class="form-control form__busqueda" placeholder="Ingresá un término de búsqueda: Sentencias, Presupuesto..." name="s" >
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form> 
                    </div>  
                    <div id="resultados"> 
                        <?php echo do_shortcode('[facetwp template="datasets"]'); ?>    
                    </div>      
                </div>
            </div>
        </div>
    </div>
    </div>
    @php 
       dynamic_sidebar( 'sidebar-top-footer' );  
    @endphp
@endsection   